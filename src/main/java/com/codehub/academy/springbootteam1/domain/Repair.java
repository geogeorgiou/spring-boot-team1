package com.codehub.academy.springbootteam1.domain;


import com.codehub.academy.springbootteam1.enums.RepairStatusEnum;
import com.codehub.academy.springbootteam1.enums.RepairTypeEnum;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "REPAIR")
public class Repair {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "repair_id")
    private Long repairId;

    @Column(name = "repair_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate repairDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "repair_status")
    private RepairStatusEnum repairStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "repair_type")
    private RepairTypeEnum repairType;

    @Column(name = "repair_cost")
    private double repairCost;

    @Column(name = "repair_address")
    private String repairAddress;

    @Column(name = "description")
    private String description;

    @ManyToOne(optional = false,cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "vat")
    private PropertyOwner propertyOwner;

    public Repair(Long repairId, LocalDate repairDate, RepairStatusEnum repairStatus,
                  RepairTypeEnum repairType, double repairCost, String repairAddress,
                  String description, PropertyOwner propertyOwner) {
        this.repairId = repairId;
        this.repairDate = repairDate;
        this.repairStatus = repairStatus;
        this.repairType = repairType;
        this.repairCost = repairCost;
        this.repairAddress = repairAddress;
        this.description = description;
        this.propertyOwner = propertyOwner;
    }

    public Repair() {
    }

    public Long getRepairId() {
        return repairId;
    }

    public void setRepairId(Long repairId) {
        this.repairId = repairId;
    }

    public LocalDate getRepairDate() {
        return repairDate;
    }

    public void setRepairDate(LocalDate repairDate) {
        this.repairDate = repairDate;
    }

    public RepairStatusEnum getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(RepairStatusEnum repairStatus) {
        this.repairStatus = repairStatus;
    }

    public RepairTypeEnum getRepairType() {
        return repairType;
    }

    public void setRepairType(RepairTypeEnum repairType) {
        this.repairType = repairType;
    }

    public double getRepairCost() {
        return repairCost;
    }

    public void setRepairCost(double repairCost) {
        this.repairCost = repairCost;
    }

    public String getRepairAddress() {
        return repairAddress;
    }

    public void setRepairAddress(String repairAddress) {
        this.repairAddress = repairAddress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PropertyOwner getPropertyOwner() {
        return propertyOwner;
    }

    public void setPropertyOwner(PropertyOwner propertyOwner) {
        this.propertyOwner = propertyOwner;
    }




    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("REPAIR{");
        sb.append("repairDate=").append(repairDate);
        sb.append(", repairStatus='").append(repairStatus).append('\'');
        sb.append(", repairType='").append(repairType).append('\'');
        sb.append("repairCost=").append(repairCost);
        sb.append(", repairAddress='").append(repairAddress).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", propertyOwner='").append(propertyOwner).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
