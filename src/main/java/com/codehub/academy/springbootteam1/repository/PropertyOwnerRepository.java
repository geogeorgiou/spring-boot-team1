package com.codehub.academy.springbootteam1.repository;

import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface PropertyOwnerRepository extends JpaRepository<PropertyOwner,Long> {


    Optional<PropertyOwner> findByEmailContaining(String email);


    @Query("SELECT p FROM PropertyOwner p " +
            "WHERE p.email LIKE CONCAT('%',?1,'%')")
    List<PropertyOwner> findByEmailContainingInOwners(String email);
}
