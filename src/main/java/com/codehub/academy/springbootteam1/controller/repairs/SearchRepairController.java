package com.codehub.academy.springbootteam1.controller.repairs;

import com.codehub.academy.springbootteam1.forms.SearchRepairForm;
import com.codehub.academy.springbootteam1.model.RepairModel;
import com.codehub.academy.springbootteam1.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
@RequestMapping("/admin/repairs")
public class SearchRepairController {

    private static final String REPAIRS_LIST = "repairs";

    @Autowired
    private RepairService repairService;

    @GetMapping(value = "")
    public String showRepairs(Model model){
        List<RepairModel> repairs = repairService.findAll();
        model.addAttribute(REPAIRS_LIST, repairs);
        return "pages/searchRepair";
    }

    //to be fixed
    @PostMapping(value = "/byVat")
    public  String showRepairsByVat(@ModelAttribute("searchRepairForm") SearchRepairForm searchRepairForm,
                                   Model model){
        List<RepairModel> repairs = repairService.findByPropertyOwnerId(Long.valueOf(searchRepairForm.getOwnerId()));
        model.addAttribute(REPAIRS_LIST,repairs);
        return "pages/searchRepair";
    }


    @PostMapping("/byDate")
    public String showRepairsByDate(@ModelAttribute("searchRepairForm") SearchRepairForm searchRepairForm,
                                    Model model){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        List<RepairModel> repairs = repairService.findByRepairDateEquals(LocalDate.parse(searchRepairForm.getRepairDate(),dateTimeFormatter));
        model.addAttribute(REPAIRS_LIST,repairs);
        return "pages/searchRepair";
    }

    @PostMapping("/byDatePeriod")
    public String showRepairsByDates(@ModelAttribute("searchRepairForm") SearchRepairForm searchRepairForm,
                                     Model model){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        LocalDate date1 = LocalDate.parse(searchRepairForm.getRepairDate(),dateTimeFormatter);
        LocalDate date2 = LocalDate.parse(searchRepairForm.getRepairDate2(),dateTimeFormatter);

        List<RepairModel> repairs = repairService.findByRepairDateBetween(date1,date2);
        model.addAttribute(REPAIRS_LIST,repairs);
        return "pages/searchRepair";
    }


}


