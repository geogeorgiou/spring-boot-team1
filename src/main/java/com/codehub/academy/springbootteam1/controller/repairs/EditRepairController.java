package com.codehub.academy.springbootteam1.controller.repairs;

import com.codehub.academy.springbootteam1.enums.RepairStatusEnum;
import com.codehub.academy.springbootteam1.enums.RepairTypeEnum;
import com.codehub.academy.springbootteam1.model.RepairModel;
import com.codehub.academy.springbootteam1.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/admin/repairs")
public class EditRepairController {

    private static final String REPAIR_ATTR = "repair";
    private static final String REPAIR_STATUS ="repairStatuses";
    private static final String REPAIR_TYPE ="repairType";


    @Autowired
    private RepairService repairService;


    @GetMapping(value = "/{id}/edit")
    public String getEditRepair(@PathVariable Long id, Model model) {
        RepairModel repairModel = repairService.findById(id);
        model.addAttribute(REPAIR_ATTR, repairModel);
        model.addAttribute(REPAIR_STATUS, RepairStatusEnum.values());
        model.addAttribute(REPAIR_TYPE, RepairTypeEnum.values());
        return "pages/editRepair";
    }

    @PostMapping(value = "/{id}")
    public String doEditRepair(@PathVariable Long id, RepairModel repairModel) {
        repairService.updateRepair(repairModel);
        return "redirect:/admin/repairs";
    }



    @PostMapping(value = "/{id}/delete")
    public String deleteRepair(@PathVariable Long id) {
        repairService.deleteById(id);
        return "redirect:/admin/repairs";
    }

}
