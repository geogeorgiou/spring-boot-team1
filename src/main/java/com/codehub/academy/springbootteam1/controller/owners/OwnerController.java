package com.codehub.academy.springbootteam1.controller.owners;

import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import com.codehub.academy.springbootteam1.enums.PropertyTypeEnum;
import com.codehub.academy.springbootteam1.enums.RoleTypeEnum;
import com.codehub.academy.springbootteam1.forms.SearchOwnerForm;
import com.codehub.academy.springbootteam1.mapper.PropertyOwnerFormToPropertyOwner;
import com.codehub.academy.springbootteam1.model.PropertyOwnerModel;
import com.codehub.academy.springbootteam1.service.PropertyOwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/admin/owners")
public class OwnerController {

    private static final String OWNERS_ATTR = "owner";
    private static final String OWNERS_PROPTYPE = "ownerPropertyTypes";
    private static final String OWNERS_ROLETYPE = "ownerRoles";


    @Autowired
    private PropertyOwnerService propertyOwnerService;

    @Autowired
    private PropertyOwnerFormToPropertyOwner mapper;

    @GetMapping({"/create"})
    public String getCreateOwner(Model model) {
        model.addAttribute(OWNERS_ATTR, new SearchOwnerForm());
        model.addAttribute(OWNERS_PROPTYPE,PropertyTypeEnum.values());
        model.addAttribute(OWNERS_ROLETYPE, RoleTypeEnum.values());
        return "pages/createOwner";
    }

    @PostMapping({"/create"})
    public String doCreateOwner(Model model,
                                @ModelAttribute(OWNERS_ATTR) SearchOwnerForm ownerForm){
//                         @Valid @ModelAttribute(OWNERS_ATTR) SearchOwnerForm ownerForm,
//                         BindingResult bindingResult){

//        if (bindingResult.hasErrors()){
//            System.out.println("binding error occured");
//            return "pages/createOwner";
//        }

        PropertyOwner propertyOwner = mapper.toPropertyOwner(ownerForm);
        propertyOwnerService.createPropertyOwner(propertyOwner);

        return "redirect:/admin/owners";
    }

    @GetMapping({"/{id}/edit"})
    public String getEditOwner(@PathVariable Long id,Model model) {
        PropertyOwnerModel propertyOwnerModel = propertyOwnerService.findByVat(id);
        model.addAttribute(OWNERS_ATTR, propertyOwnerModel);
        model.addAttribute(OWNERS_PROPTYPE, PropertyTypeEnum.values());
        return "/pages/editOwner";
    }

    @PostMapping({"/{id}"})
    public String doEditOwner(@PathVariable Long id, PropertyOwnerModel propertyOwnerModel) {
        propertyOwnerService.updateOwner(propertyOwnerModel);
        return "redirect:/admin/owners";
    }

    @PostMapping({"/{id}/delete"})
    public String deleteOwner(@PathVariable Long id) {
        propertyOwnerService.deleteByVat(id);
        return "redirect:/admin/owners";
    }

}
