package com.codehub.academy.springbootteam1.mapper;


import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import com.codehub.academy.springbootteam1.enums.PropertyTypeEnum;
import com.codehub.academy.springbootteam1.enums.RepairStatusEnum;
import com.codehub.academy.springbootteam1.enums.RepairTypeEnum;
import com.codehub.academy.springbootteam1.enums.RoleTypeEnum;
import com.codehub.academy.springbootteam1.model.PropertyOwnerModel;
import com.codehub.academy.springbootteam1.model.RepairModel;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;

@Component
public class PropertyOwnerToPropertyOwnerModel {

    public PropertyOwnerModel mapToPropertyOwnerModel(PropertyOwner owner){

        PropertyOwnerModel ownerModel= new PropertyOwnerModel();

        ownerModel.setAddress(owner.getAddress());
        ownerModel.setEmail(owner.getEmail());
        ownerModel.setFirstName(owner.getFirstName());
        ownerModel.setLastName(owner.getLastName());
        ownerModel.setPhoneNumber(String.valueOf(owner.getPhoneNumber()));
        ownerModel.setPropertyType(owner.getPropertyType() != null ? owner.getPropertyType() : PropertyTypeEnum.DEFAULT);
        ownerModel.setPassword(owner.getPassword());
        ownerModel.setRole(owner.getRole() != null ? owner.getRole() : RoleTypeEnum.USER); //USER IS THE DEFAULT OPTION
        ownerModel.setVat(String.valueOf(owner.getVat()));

        return ownerModel;
    }


}
