package com.codehub.academy.springbootteam1.enums;

public enum  RepairStatusEnum {

    DEFAULT,
    PENDING,
    PROGRESS,
    COMPLETED;

}


