package com.codehub.academy.springbootteam1.service;

import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import com.codehub.academy.springbootteam1.model.PropertyOwnerModel;

import java.util.List;
import java.util.Optional;

public interface PropertyOwnerService {

    //SEARCH

    PropertyOwnerModel findByVat(Long vat);

    Optional<PropertyOwner> findById(Long id);

    PropertyOwnerModel findByEmailContaining(String email);

    List<PropertyOwnerModel> findByEmailContainingInOwners(String email);

    List<PropertyOwnerModel> findAll();

    //DELETE

    void deleteByVat(Long id);

    //UPDATE

    PropertyOwner updateOwner(PropertyOwnerModel ownerModel);

    //CREATE

    void createPropertyOwner(PropertyOwner propertyOwner);

}
